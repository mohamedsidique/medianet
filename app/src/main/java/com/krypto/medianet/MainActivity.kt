package com.krypto.medianet

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.krypto.medianet.ui.fragments.HomeFragment
import com.krypto.medianet.utils.MNConstants

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.main_container, HomeFragment(), MNConstants.HOME_TAG).commitAllowingStateLoss()
    }
}
