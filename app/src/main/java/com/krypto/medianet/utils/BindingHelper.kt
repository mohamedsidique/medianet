package com.krypto.medianet.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

object BindingHelper {
    @JvmStatic
    @BindingAdapter("bind:imageUrl","bind:picasso")
    fun loadImage(imageView: ImageView, url: String, picasso: Picasso) {
        if (url != "") {
            picasso.load(url).into(imageView)
        }
    }
}