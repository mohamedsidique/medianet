package com.krypto.medianet.injection.module

import com.krypto.medianet.ui.viewmodels.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsModule = module {
    viewModel {
        HomeViewModel(newsAPI = get())
    }
}