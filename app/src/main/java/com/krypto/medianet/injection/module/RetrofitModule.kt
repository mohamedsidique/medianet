package com.krypto.medianet.injection.module

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.krypto.medianet.network.NewsAPI
import com.krypto.medianet.utils.MNConstants
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import okhttp3.MediaType.Companion.toMediaType

private const val CACHE_FILE_SIZE: Long = 30 * 1000 * 1000

val retrofitModule = module {    // 1

    single {   // 2
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        okHttp(interceptor)  // 3
    }
    single {

        retrofit(MNConstants.BASE_URL)  // 4
    }
    single {
        get<Retrofit>().create(NewsAPI::class.java)   // 5
    }
}
private fun okHttp(interceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
    .addInterceptor(interceptor)
    .build()
private fun retrofit(baseUrl: String) = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(Json(JsonConfiguration(strictMode = false)).asConverterFactory("application/json".toMediaType()))  // 6
        .build()
