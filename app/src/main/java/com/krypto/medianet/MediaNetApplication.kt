package com.krypto.medianet

import android.app.Application
import com.krypto.medianet.injection.module.newsModule
import com.krypto.medianet.injection.module.picassoModule
import com.krypto.medianet.injection.module.retrofitModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MediaNetApplication  : Application(){
    override fun onCreate() {
        super.onCreate()
        startKoin {   // 1
            androidLogger(Level.DEBUG)  // 2
            androidContext(this@MediaNetApplication)  // 3
            modules(listOf(retrofitModule, picassoModule, newsModule))  // 4
        }
    }
}