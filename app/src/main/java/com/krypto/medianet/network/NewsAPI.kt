package com.krypto.medianet.network

import com.krypto.medianet.model.news.NewsResponse
import com.krypto.medianet.model.weather.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NewsAPI {
    //news
    @GET(value = "news")
    suspend fun newsData() :NewsResponse

    //weather?latitude=17&longitude=73
    @GET(value = "weather")
    suspend fun weatherNews(@Query(value = "latitude") latitude: Double, @Query(value = "longitude") longitude: Double) : WeatherResponse
}