package com.krypto.medianet.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.krypto.medianet.R
import com.krypto.medianet.base.BaseFragment
import com.krypto.medianet.databinding.HomeFragmentScreenBinding
import com.krypto.medianet.ui.adapter.HomePageRecyclerAdapter
import com.krypto.medianet.ui.viewmodels.HomeViewModel
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : BaseFragment(){
    private val newsViewModel: HomeViewModel by viewModel()
    private val picasso: Picasso by inject()

    lateinit var binding: HomeFragmentScreenBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment_screen,container,false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = HomePageRecyclerAdapter(picasso = picasso)
        binding.recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            this.adapter = adapter
        }

//        newsViewModel.uiState.observe(viewLifecycleOwner, Observer {
//            val dataState = it ?: return@Observer
//            binding.progressBar.visibility = if (dataState.showProgress) View.VISIBLE else View.GONE
//            if (dataState.news != null && !dataState.news.consumed)
//                dataState.news.consume()?.let { news ->
//                    adapter.submitList(news)
//                }
//            if(dataState.error != null && !dataState.error.consumed)
//                dataState.error.consume()?.let { errorMessage ->
//                   Log.d("error",errorMessage)
//                }
//        })

        newsViewModel.liveItems.observe(viewLifecycleOwner, Observer {
            val dataState = it ?: return@Observer
            if(dataState.size > 0){
                binding.progressBar.visibility = View.INVISIBLE
                adapter.submitList(dataState)
                adapter.notifyDataSetChanged()
            }
        })

    }
}