package com.krypto.medianet.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.krypto.medianet.BR
import com.krypto.medianet.R
import com.krypto.medianet.databinding.NewsItemLargeBinding
import com.krypto.medianet.databinding.NewsItemSmallRowBinding
import com.krypto.medianet.databinding.WeatherRowItemBinding
import com.krypto.medianet.model.news.NewsItem
import com.krypto.medianet.model.weather.WeatherBody
import com.squareup.picasso.Picasso


class HomePageRecyclerAdapter(val picasso: Picasso) :
    ListAdapter<Any, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Any>() {
            override fun areItemsTheSame(
                oldItem: Any,
                newItem: Any
            ) = if (oldItem is NewsItem && newItem is NewsItem) {

                oldItem.aid == newItem.aid
            } else {
                oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: Any,
                newItem: Any
            ) = if (oldItem is NewsItem && newItem is NewsItem) {
                oldItem.aid == newItem.aid
            } else {
                false
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val layout = when (viewType) {
            0 -> R.layout.weather_row_item
            1 -> R.layout.news_item_small_row
            2 -> R.layout.news_item_large
            else -> R.layout.news_item_small_row
        }

        return when (viewType) {
            0 -> {
                val view =
                    DataBindingUtil.inflate<WeatherRowItemBinding>(inflater, layout, parent, false)
                WeatherNewsViewHolder(view,picasso)
            }
            1 -> {
                val view = DataBindingUtil.inflate<NewsItemSmallRowBinding>(
                    inflater,
                    layout,
                    parent,
                    false
                )
                SmallNewsViewHolder(view,picasso)
            }
            else -> {
                val view = DataBindingUtil.inflate<NewsItemLargeBinding>(
                    inflater,
                    layout,
                    parent,
                    false
                )
                LargeNewsViewHolder(view,picasso)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            getItem(position) is WeatherBody -> {
                0
            }
            position %3 == 0 -> {
                2
            }
            else -> {
                1
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = getItem(position)
        when (holder.itemViewType) {
            0 -> {
                val weatherHolder = holder as WeatherNewsViewHolder
                weatherHolder.bind(model as WeatherBody)
            }
            1 -> {
                val smallNewsHolder = holder as SmallNewsViewHolder
                smallNewsHolder.bind(model as NewsItem)
            }
            2 -> {
                val largeNewsHolder = holder as LargeNewsViewHolder
                largeNewsHolder.bind(model as NewsItem)
            }
        }
    }

    class SmallNewsViewHolder(val binding: NewsItemSmallRowBinding,val picasso: Picasso) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: NewsItem) {

            binding.setVariable(
                BR.newsitem,
                data
            ) //BR - generated class; BR.user -- 'user' is variable name declared in layout
            binding.setVariable(
                BR.picasso,
                picasso
            ) //BR - generated class; BR.user -- 'user' is variable name declared in layout
            binding.executePendingBindings()
        }
    }

    class WeatherNewsViewHolder(val binding: WeatherRowItemBinding,val picasso: Picasso) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: WeatherBody) {

            binding.setVariable(
                BR.weatherbody,
                data
            ) //BR - generated class; BR.user -- 'user' is variable name declared in layout
            binding.setVariable(
                BR.picasso,
                picasso
            ) //BR - generated class; BR.user -- 'user' is variable name declared in layout
            binding.executePendingBindings()
        }
    }

    class LargeNewsViewHolder(val binding: NewsItemLargeBinding,val picasso: Picasso) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: NewsItem) {

            binding.setVariable(
                BR.newsitem,
                data
            ) //BR - generated class; BR.user -- 'user' is variable name declared in layout
            binding.setVariable(
                BR.picasso,
                picasso
            ) //BR - generated class; BR.user -- 'user' is variable name declared in layout
            binding.executePendingBindings()
        }
    }
}