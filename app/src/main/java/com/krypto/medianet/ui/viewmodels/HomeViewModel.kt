package com.krypto.medianet.ui.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.krypto.medianet.model.news.NewsItem
import com.krypto.medianet.network.NewsAPI
import com.krypto.medianet.utils.Event
import kotlinx.coroutines.launch

class HomeViewModel(private val newsAPI: NewsAPI) : ViewModel(){
    private val _uiState = MutableLiveData<HomeDataState>()
    val uiState: LiveData<HomeDataState> get() = _uiState

    val liveItems : MutableLiveData<ArrayList<Any?>>

    init {
        liveItems = MutableLiveData()
        liveItems.value = ArrayList()
        retrieveNews()
        retrieveWeatherNews()
    }

    fun retrieveNews() {
        viewModelScope.launch {
            runCatching {
//                emitUiState(showProgress = true)
                newsAPI.newsData()
            }.onSuccess {
                if(true == it.status){
//                    emitUiState(news = Event(it.newsBody?.newsItems))
                    val newsBody = it.newsBody!!
                    addItems(newsBody.newsItems!!)
                }
            }.onFailure {
                Log.d("error",it.message)
//                emitUiState(error = Event(it.message!!))
            }
        }
    }

    fun retrieveWeatherNews() {
        viewModelScope.launch {
            runCatching {
//                emitUiState(showProgress = true)
                newsAPI.weatherNews(latitude = 17.0,longitude = 73.0)
            }.onSuccess {
                if(true == it.status){
                    addItem(0,it.weatherBody!!)
                }
            }.onFailure {
                Log.d("error",it.message)

//                emitUiState(error = Event(it.message!!))
            }
        }
    }

    private fun emitUiState(
        showProgress : Boolean = false,
        news : Event<List<NewsItem?>?>? = null,
        error : Event<String>? = null
    ) {
        val dataState = HomeDataState(showProgress, news, error)
        _uiState.value = dataState
    }

    data class HomeDataState(
        val showProgress : Boolean,
        val news : Event<List<NewsItem?>?>?,
        val error : Event<String>?
    )

    fun addItem(index:Int,item: Any) {
        liveItems.value?.add(index,item)
        liveItems.value = liveItems.value
    }

    fun addItems(items: List<Any>) {
        liveItems.value?.addAll(items)
        liveItems.value = liveItems.value
    }
}
