package com.krypto.medianet.base

import androidx.fragment.app.Fragment

open class BaseFragment : Fragment()