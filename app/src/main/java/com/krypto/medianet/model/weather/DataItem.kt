package com.krypto.medianet.model.weather


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class DataItem(

	@SerialName(value="summary")
	val summary: String? = null,

	@SerialName    (value="precipProbability")
	val precipProbability: Double? = null,

	@SerialName    (value="visibility")
	val visibility: Double? = null,

	@SerialName    (value="windGust")
	val windGust: Double? = null,

	@SerialName    (value="precipIntensity")
	val precipIntensity: Double? = null,

	@SerialName    (value="cloudCover")
	val cloudCover: Double? = null,

	@SerialName    (value="icon")
	val icon: String? = null,

	@SerialName    (value="windBearing")
	val windBearing: Int? = null,

	@SerialName    (value="apparentTemperature")
	val apparentTemperature: Double? = null,

	@SerialName    (value="pressure")
	val pressure: Double? = null,

	@SerialName    (value="dewPoint")
	val dewPoint: Double? = null,

	@SerialName    (value="ozone")
	val ozone: Double? = null,

	@SerialName    (value="temperature")
	val temperature: Double? = null,

	@SerialName    (value="humidity")
	val humidity: Double? = null,

	@SerialName    (value="time")
	val time: Int? = null,

	@SerialName    (value="uvIndex")
	val uvIndex: Int? = null,

	@SerialName    (value="windSpeed")
	val windSpeed: Double? = null,

	@SerialName    (value="precipType")
	val precipType: String? = null
)