package com.krypto.medianet.model.news

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class RaItem(

	@SerialName(value="imglg")
	val imglg: String? = null,

//	@SerialName    (value="auth")
//	val auth: Any? = null,

	@SerialName    (value="ty")
	val ty: Int? = null,

	@SerialName    (value="acCats")
	val acCats: String? = null,

	@SerialName    (value="pv")
	val pv: Int? = null,

	@SerialName    (value="b1")
	val b1: String? = null,

	@SerialName    (value="disptpcs")
	val disptpcs: String? = null,

	@SerialName    (value="atd")
	val atd: Int? = null,

	@SerialName    (value="b2")
	val b2: String? = null,

	@SerialName    (value="dncategory")
	val dncategory: String? = null,

	@SerialName    (value="ccategory")
	val ccategory: String? = null,

	@SerialName    (value="ctime")
	val ctime: String? = null,

	@SerialName    (value="hvids")
	val hvids: Int? = null,

	@SerialName    (value="if")
	val jsonMemberIf: Boolean? = null,

	@SerialName    (value="imgt")
	val imgt: String? = null,

	@SerialName    (value="fbc")
	val fbc: String? = null,

	@SerialName    (value="imgsm")
	val imgsm: String? = null,

//	@SerialName    (value="em")
//	val em: Any? = null,

	@SerialName    (value="acategory")
	val acategory: String? = null,

//	@SerialName    (value="ra")
//	val ra: List<Any?>? = null,

//	@SerialName    (value="tags")
//	val tags: Any? = null,

	@SerialName    (value="bsc")
	val bsc: String? = null,

	@SerialName    (value="pubd")
	val pubd: String? = null,

	@SerialName    (value="hdump")
	val hdump: String? = null,

	@SerialName    (value="nc")
	val nc: Int? = null,

	@SerialName    (value="ycname")
	val ycname: String? = null,

	@SerialName    (value="imgl")
	val imgl: String? = null,

	@SerialName    (value="rawCatId")
	val rawCatId: String? = null,

	@SerialName    (value="sty")
	val sty: Int? = null,

	@SerialName    (value="subType")
	val subType: String? = null,

	@SerialName    (value="aid")
	val aid: Long? = null,

	@SerialName    (value="imgr")
	val imgr: String? = null,

	@SerialName    (value="cid")
	val cid: String? = null,

	@SerialName    (value="desc")
	val desc: String? = null,

	@SerialName    (value="yvid")
	val yvid: String? = null,

	@SerialName    (value="bst")
	val bst: String? = null,

	@SerialName    (value="img")
	val img: String? = null,

	@SerialName    (value="bss")
	val bss: String? = null,

	@SerialName    (value="b1t")
	val b1t: String? = null,

	@SerialName    (value="bf")
	val bf: Boolean? = null,

	@SerialName    (value="bsu")
	val bsu: String? = null,
//
//	@SerialName    (value="d_img")
//	val dImg: Any? = null,

	@SerialName    (value="ns")
	val ns: Int? = null,

	@SerialName    (value="nv")
	val nv: Int? = null,

	@SerialName    (value="csize")
	val csize: Int? = null,

	@SerialName    (value="sec")
	val sec: String? = null,

//	@SerialName    (value="sd")
//	val sd: Any? = null,

	@SerialName    (value="isvid")
	val isvid: Int? = null,

	@SerialName    (value="dcount")
	val dcount: String? = null,

	@SerialName    (value="imgid")
	val imgid: Int? = null,

	@SerialName    (value="ch")
	val ch: String? = null,

//	@SerialName    (value="esCScore")
//	val esCScore: Any? = null,

	@SerialName    (value="scids")
	val scids: String? = null,

	@SerialName    (value="dicon")
	val dicon: String? = null,

	@SerialName    (value="dimg")
	val dimg: String? = null,

	@SerialName    (value="site")
	val site: String? = null,

	@SerialName    (value="dbtime")
	val dbtime: String? = null,

	@SerialName    (value="t")
	val T: String? = null,

	@SerialName    (value="u")
	val U: String? = null,

	@SerialName    (value="etime")
	val etime: String? = null,

	@SerialName    (value="yblr")
	val yblr: String? = null,

	@SerialName    (value="iscore")
	val iscore: Double? = null,

	@SerialName    (value="updtime")
	val updtime: String? = null,

	@SerialName    (value="cscore")
	val cscore: Double? = null,

//	@SerialName    (value="pub")
//	val pub: Any? = null,

	@SerialName    (value="fscore")
	val fscore: Double? = null,

	@SerialName    (value="dc")
	val dc: String? = null
)