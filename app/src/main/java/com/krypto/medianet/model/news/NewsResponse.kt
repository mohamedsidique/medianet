package com.krypto.medianet.model.news

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class NewsResponse(

	@SerialName(value="message")
	val message: String? = null,

	@SerialName(value="body")
	val newsBody: NewsBody? = null,

	@SerialName(value="status")
	val status: Boolean? = null
)