package com.krypto.medianet.model.weather


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeatherResponse(

	@SerialName(value="message")
	val message: String? = null,

	@SerialName    (value="body")
	val weatherBody: WeatherBody? = null,

	@SerialName    (value="status")
	val status: Boolean? = null
)