package com.krypto.medianet.model.weather


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Daily(

    @SerialName(value="summary")
	val summary: String? = null,

    @SerialName    (value="data")
	val data: List<DataItem?>? = null,

    @SerialName    (value="icon")
	val icon: String? = null
)