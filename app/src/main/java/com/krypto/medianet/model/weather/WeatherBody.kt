package com.krypto.medianet.model.weather


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class WeatherBody(

	@SerialName(value="currently")
	val currently: Currently? = null,

	@SerialName    (value="offset")
	val offset: Double? = null,

	@SerialName    (value="timezone")
	val timezone: String? = null,

	@SerialName    (value="daily")
	val daily: Daily? = null,

	@SerialName    (value="latitude")
	val latitude: Double? = null,

	@SerialName    (value="flags")
	val flags: Flags? = null,

	@SerialName    (value="hourly")
	val hourly: Hourly? = null,

	@SerialName    (value="abbr")
	val abbr: String? = null,

	@SerialName    (value="longitude")
	val longitude: Double? = null
){

	fun getMinMaxTemperature():String{
		var minTemp = Double.MAX_VALUE
		var maxTemp = Double.MIN_VALUE

		hourly?.data?.forEach {
			val temporaryTemp = it?.temperature!!
			if(temporaryTemp < minTemp){
				minTemp = temporaryTemp
			}else if(temporaryTemp > maxTemp){
				maxTemp = temporaryTemp
			}
		}
		return "$minTemp ${0x00B0.toChar()}C / $maxTemp ${0x00B0.toChar()}C"
	}

	fun getHumidityData() :String{
		var humidity = currently?.humidity!!
		humidity*=100
		return "${humidity.toInt()}% today"
	}

	fun getCurrentTemp() :String{
		return "${currently?.temperature} ${0x00B0.toChar()}C"
	}
}