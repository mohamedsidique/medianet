package com.krypto.medianet.model.weather


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class Flags(

	@SerialName(value="nearest-station")
	val nearestStation: Double? = null,

	@SerialName    (value="sources")
	val sources: List<String?>? = null,

	@SerialName    (value="units")
	val units: String? = null
)