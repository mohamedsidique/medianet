package com.krypto.medianet.model.news


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class NewsBody(

	@SerialName(value= "size")
	val size: Int? = null,

	@SerialName    (value="resultSet")
	val newsItems: List<NewsItem>? = null,

	@SerialName    (value="status")
	val status: String? = null
)